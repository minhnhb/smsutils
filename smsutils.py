import re

VNP_REGEX = "^(8491|8494|84123|84124|84125|84127|84129)"
VMS_REGEX = "^(8490|8493|84120|84121|84122|84126|84128)"
VTL_REGEX = "^(8498|8497|8496|84165|84166|84167|84168|84169|84164|84163|84162)"
SFE_REGEX = "^8495"
VNM_REGEX = "^(84186|84188|8492)"
GMB_REGEX = "^(84199|84993|84994|84995|84996|84997)"

def detect_telco(msisdn):
    if re.match(VNP_REGEX, msisdn):
        return "vinaphone"
    elif re.match(VMS_REGEX, msisdn):
        return "mobifone"
    elif re.match(VTL_REGEX, msisdn):
        return "viettel"
    elif re.match(SFE_REGEX, msisdn):
        return "sfone"
    elif re.match(VNM_REGEX, msisdn):
        return "vietnammobile"
    elif re.match(GMB_REGEX, msisdn):
        return "gmobile"

    return "other"

def get_billing_from_shortcode(shortcode):
    # Apply for shortcodes like 8x38, 8x33, 6x86, ...
    amounts = [500, 1000, 2000, 3000, 4000,\
               5000, 10000, 15000]

    try:
        index = int(shortcode[1])
        return amounts[index]
    except:
        return 0
